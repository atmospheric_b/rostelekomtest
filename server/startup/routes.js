const imagesRouter = require('../routes/images');
const usersRouter = require('../routes/users');
const config = require('../connectors/config');

module.exports = (app) => {
	app.use(`${config.get('express:prefix')}/images`, imagesRouter); // роутер картинок
	app.use(`${config.get('express:prefix')}/user`, usersRouter); // роутер пользователей
};
