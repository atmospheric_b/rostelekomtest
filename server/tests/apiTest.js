const request = require('supertest');
const app = require('../server');
const config = require('../connectors/config');
const expect = require('chai').expect;
//==================== images API test ====================

/**
 * Testing get images endpoint
 */
describe('GET /images/4', function () {
    it('respond with json array containing a list 4 images', function (done) {
        request(app)
            .get(`${config.get('express:prefix')}/images/4`)
            .set('Accept', 'application/json')
            .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGFkbWluLmNvbSIsInVzZXJJZCI6MSwiaWF0IjoxNTY0NjA0ODUxfQ.4UREWrgN-uKBRKZHXrVMTGnK9KXtE_sFxaXqfgDzXgo')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
                console.log(res.body);
                if (err) return done(err);
                expect(res.body.data).to.be.an.instanceof(Array);
                expect(res.body.data).to.have.lengthOf(4);
                done();
            });

    });
});

describe('GET /images/16', function () {
    it('respond with json array containing a list 16 images', function (done) {
        request(app)
            .get(`${config.get('express:prefix')}/images/16`)
            .set('Accept', 'application/json')
            .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGFkbWluLmNvbSIsInVzZXJJZCI6MSwiaWF0IjoxNTY0NjA0ODUxfQ.4UREWrgN-uKBRKZHXrVMTGnK9KXtE_sFxaXqfgDzXgo')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
                console.log(res.body);
                if (err) return done(err);
                expect(res.body.data).to.be.an.instanceof(Array);
                expect(res.body.data).to.have.lengthOf(16);
                done();
            });

    });
});