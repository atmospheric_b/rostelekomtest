const express = require('express');
const Router = express.Router();
const db = require('../db/db');
const fs = require('fs');
const config = require('../connectors/config');
const verifyToken = require('../helper/jwtHelper');


// Запрос на картинки по количеству начиная с 4х
Router.get('/:id', verifyToken, async (req, res) => {
    let count;
    if(!req.params.id) count = 4;
    else count = req.params.id;
    let data = [];
    db.reload();
    const dbImagesArr = Array.from(db.getData(`/imageGallery/images`));
    const totalImages = dbImagesArr.length;
    for(let i = 0; i < count; i++){
        if(dbImagesArr.indexOf(dbImagesArr[i]) !== -1){
            data.push(dbImagesArr[i]);
        }
    }
    return res.status(200).json({result: 'success', data, totalImages})
});

// Загрузка картинки из админки
Router.post('/upload', verifyToken, async (req, res) => {
    if (req.files === null) {
        return res.status(400).json({result: 'error', data: 'No file uploaded'});
    }

    const {body} = req;
    const {title, desc} = body;
    const file = req.files.file;
    let id = Math.max.apply(Math, db.getData("/imageGallery/images").map((o) => { return o.id; })) + 1; // Получаем последний айди из базы данных
    const fullName = `${id}.${file.name.split('.')[file.name.split('.').length-1]}`;
    const src = `http://localhost:9006/static/${fullName}`;
    //Перемещаем файл в директорию со статическими файлами и при успехе пушим обновление в базу
    return file.mv(`${__dirname}/../images/${fullName}`, err => {
        if (err) {
            console.error(err);
            return res.status(500).send(err);
        }

        // Формируем объект для базы
        const dbData = {};

        dbData['id'] = id;
        dbData['src'] = src;
        dbData['title'] = title;
        dbData['desc'] = desc;

        db.push("/imageGallery/images[]", dbData, true); // Пушим инфу о новом изображении
        db.reload();
        return res.status(200).json({
            result: 'success',
            data: {fileName: file.name, filePath: `${config.get('express:imageUrl')}${id}`}
        });
    });
});

Router.delete('/:id', verifyToken, async (req, res) => {
    try{
        if(!req.params.id) return res.status(200).json({result: "error", data: 'no data received'});
        const id = parseInt(req.params.id, 10);
        const image = Array.from(db.getData("/imageGallery/images")).find((image, index) => {
            return image.id === id
        });
        let imageId = image.id;
        let fullName = `${imageId}.${image.src.split('.')[image.src.split('.').length-1]}`;
        if(!image) return res.status(200).json({result: 'error', data: 'Image not found'});
        db.delete(`/imageGallery/images[${imageId-1}]`);
        fs.unlinkSync(`${__dirname}/../images/${fullName}`);
        db.reload();
    } catch (e) {
        console.log(e);
        return res.status(200).json({result: 'error', data: 'Error on image delete'})
    }
    return res.status(200).json({result: 'success', data: 'Image deleted'})
});


module.exports = Router;