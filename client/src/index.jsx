import 'react-app-polyfill/ie9';
import 'react-app-polyfill/stable';
import "core-js";
import React from 'react';
import {render} from 'react-dom';
import * as serviceWorker from './serviceWorker';
import './Styles/main.css';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider'
import theme from './Theme';
import Routes from './Routes';

// Обернем наши компоненты в MuiThemeProvider для того чтобы тема всегда была с нами
const App = () => {
    return (
        <MuiThemeProvider theme={theme}>
            <Routes/>
        </MuiThemeProvider>
    )
};

render(<App/>, document.getElementById('root') || document.createElement('div'));

serviceWorker.unregister();

export default App;
