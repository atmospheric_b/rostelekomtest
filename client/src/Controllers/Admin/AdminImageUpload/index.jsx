import React, {Component} from 'react';
import PropTypes from 'prop-types';
import AdminImageUploadComponent from "../../../Components/Admin/AdminImageUploadComponent";
import MainLayout from "../../../Components/Layouts/MainLayout";


// Контроллер для админа, для загрузки новых картинок
class AdminImageUpload extends Component {
    state = {
        title: '',
        desc: '',
        loading: false,
    };



    changeTitle = (title) => {
        this.setState({title})};
    changeDesc = (desc) => {this.setState({desc})};
    changeLoading = (loading) => {this.setState({loading: loading})};


    render() {
        const {title, desc, loading} = this.state;
        return (
            <MainLayout>
                <AdminImageUploadComponent
                    title={title}
                    desc={desc}
                    loading={loading}
                    changeTitle={this.changeTitle}
                    changeLoading={this.changeLoading}
                    changeDesc={this.changeDesc}
                />
            </MainLayout>
        );
    }
}

AdminImageUpload.propTypes = {
    location: PropTypes.object,
};

export default AdminImageUpload;