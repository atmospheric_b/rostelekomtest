import React, {Component, lazy, Suspense} from 'react';
import MainLayout from '../../Components/Layouts/MainLayout'
import apiRequest from '../../Helpers/apiRequest'
import {IMAGES} from '../../config'
import Swal from "sweetalert2";
import Loading from "../../Components/Loading";

class PictureGallery extends Component {
    state = {
        images: [],
        totalImages: 0,
        alreadyDownloaded: 0,
        ref: null,
        noImagesLeft: false,
    };


    componentDidMount() {
        apiRequest(`${IMAGES}/4`,'GET',{}, {}).then(({data}) => {
            if(data.result === 'success') this.setState({images: data.data, totalImages: data.totalImages, alreadyDownloaded: 4})
        })

    }

    deleteImage = (id) => {
        Swal.fire({
            title: 'Вы уверены?',
            text: "Это действие безвозвратно",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#ff3366',
            cancelButtonColor: '#28282a',
            confirmButtonText: 'Да удалить!',
            cancelButtonText: 'Отмена!'
        }).then(async (result) => {
            if (result.value) {
                apiRequest(`${IMAGES}/${id}`,'DELETE',{}, {}).then(({data}) => {
                    if(data.result === 'success') this.setState(prevState => ({
                        images: prevState.images.filter((el) => el.id !== id)
                    }))
                })
            }
        });
    };

    // загружаем по 4 ведя учёт количества загрузок alreadyDownloaded
    downloadMoreImages = async () => {
        apiRequest(`${IMAGES}/${this.state.alreadyDownloaded+4}`,'GET', {}, {}).then(({data}) => {
            if(data.result === 'success') {
                // Проверка остались ли еще картинки в бд
                if(JSON.stringify(data.data) !== JSON.stringify(this.state.images)){
                    this.setState({images: data.data, totalImages: data.totalImages, alreadyDownloaded: this.state.alreadyDownloaded+4})
                }else{
                    Swal.fire({
                        confirmButtonColor: '#ff3366',
                        titleText: 'Внимание!',
                        text: 'Больше нет изображений',
                        type: 'warning'
                    });
                    this.setState({noImagesLeft: true})
                }

            }
        })
    };

    render() {
        const PictureGalleryComponent = lazy(() => import('../../Components/User/PictureGallery/PictureGalleryComponent'));
        const {images, totalImages,noImagesLeft,} = this.state;
        return (
            <MainLayout>
                <Suspense fallback={<Loading/>}>
                    <PictureGalleryComponent
                        images={images}
                        totalImages={totalImages}
                        noImagesLeft={noImagesLeft}
                        deleteImage={this.deleteImage}
                        downloadMoreImages={this.downloadMoreImages}
                    />
                </Suspense>
            </MainLayout>
        );
    }
}



export default PictureGallery;

