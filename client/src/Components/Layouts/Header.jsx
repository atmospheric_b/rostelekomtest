import React from 'react';
import {withRouter} from 'react-router-dom';
import {makeStyles} from '@material-ui/core/styles';
import {AppBar, IconButton, Toolbar, Typography} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import ExitToApp from '@material-ui/icons/ExitToApp';
import Grid from "@material-ui/core/Grid";
import PropTypes from "prop-types";

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
        fontWeight: 600,
    },
    redText: {
        color: 'red',
        fontWeight: 600,
    },
    greenText: {
        color: '#4BB543',
        fontWeight: 600,
    }
}));

const Header = props => {
    const {toggleDrawer, logout} = props;
    const classes = useStyles();
    const userLocal = JSON.parse(localStorage.getItem('user'));
    return (
        <div className={classes.root}>
            <AppBar position="static" color={"primary"}>
                <Toolbar>
                    <IconButton edge="start" className={classes.menuButton} id="menuOpen" color="inherit" aria-label="Menu"
                                onClick={toggleDrawer(true)}>
                        <MenuIcon color='secondary'/>
                    </IconButton>
                    <Typography variant="h6" color='secondary' className={classes.title}>
                        {props.location.pathname === '/picture-gallery' && 'Галерея'}
                        {props.location.pathname === '/admin-image-upload' && 'Загрузка картинки для админа'}
                    </Typography>
                    <Grid>
                        {userLocal && <>
                        <Typography variant="h6" color='secondary' className={classes.title}>
                            <Typography className={classes.listTitle}> {userLocal.email.split('@')[0]}</Typography>
                        </Typography></>}
                    </Grid>
                    <IconButton color="secondary" className={classes.button} onClick={logout}>
                        <ExitToApp color="secondary"/>
                        <Typography className={classes.listTitle}>Выход</Typography>
                    </IconButton>
                </Toolbar>
            </AppBar>
        </div>
    );
};


export default withRouter(Header);

Header.propTypes = {
    toggleDrawer: PropTypes.func,
    history: PropTypes.object,
    logout: PropTypes.func,
};