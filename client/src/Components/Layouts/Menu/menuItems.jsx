import React from 'react';
import {
    Divider,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    Typography,
} from '@material-ui/core';


import {
    AddToPhotos,
    ExitToApp,
    Collections,
} from '@material-ui/icons';





const menuItems = (toggleDrawer, classes, logout, expanded, setExpanded, handleChange, typeOfUser) => (
    <div
        className={classes.list}
        role="presentation"
    >
        <Typography variant="h4" color='primary' className={classes.title}>
            Меню
        </Typography>
        <Divider />
        <List className={classes.menuList}>
            {typeOfUser === 'user' ? <ListItem button onClick={toggleDrawer(false, 'picture-gallery')}>
                <ListItemIcon>
                    <Collections/>
                </ListItemIcon>
                <ListItemText primary={
                    <Typography className={classes.listTitle}>Галерея</Typography>
                }/>
            </ListItem> : <>
                <ListItem button onClick={toggleDrawer(false, 'admin-image-upload')}>
                    <ListItemIcon>
                        <AddToPhotos/>
                    </ListItemIcon>
                    <ListItemText primary={
                        <Typography className={classes.listTitle}>Загрузка картинок</Typography>
                    }/>
                </ListItem>
                <ListItem button onClick={toggleDrawer(false, 'picture-gallery')}>
                    <ListItemIcon>
                        <Collections/>
                    </ListItemIcon>
                    <ListItemText primary={
                        <Typography className={classes.listTitle}>Галерея</Typography>
                    }/>
                </ListItem>
                </>
            }

            <Divider />
        </List>
        <Divider />
        <List>
            <ListItem button onClick={logout}>
                <ListItemIcon>
                        <ExitToApp />
                </ListItemIcon>
                <Typography className={classes.listTitle}>Выход</Typography>
            </ListItem>
        </List>
    </div>
);

export default menuItems;