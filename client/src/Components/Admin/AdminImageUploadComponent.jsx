import React, {useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import UploadModal from '../Modals/UploadModal';
import {
    Button,
    CircularProgress,
    Grid,
    Paper,
    TextField,
    Typography,
} from '@material-ui/core';

import PropTypes from "prop-types";



const useStyles = makeStyles(theme => ({
    layout: {
        width: 'auto',
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
            width: 800,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
        minHeight: 800,
        marginTop: 50,
    },
    progress:{
        margin: 'auto'
    },
    paper: {
        padding: 20,
        margin: 20,
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
    },
    buttonGroup: {
        display: "flex",
        justifyContent: "center",
        flexWrap: 'wrap',
        maxWidth: 800
    },
    oneOfButtonsGrid:{
        width: 205,
        margin: 20
    },
}));

// Пока не будут заполнены поля кнопка с отправкой будет выключена
function AdminImageUploadComponent(props) {
    const classes = useStyles();
    const [uploadModalOpen, setUploadModal] = useState(false);

    const {
        title,
        desc,
        loading,
        changeTitle,
        changeDesc,
        changeLoading,
    } = props;

    return (
        <main className={classes.layout}>
            <Paper className={classes.paper}>
                <Typography variant="h5" component="h3">
                    Загрузить картинку
                </Typography>
                <br/>
                <Grid container spacing={3}>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            id="title"
                            name="title"
                            label="Название"
                            value={title}
                            onChange={(e) => {changeTitle(e.target.value)}}
                            fullWidth
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            id="desc"
                            name="desc"
                            label="Описание"
                            value={desc}
                            onChange={(e) => {changeDesc(e.target.value)}}
                            fullWidth
                        />
                    </Grid>
                    {!loading && <Grid item xs={12} className={classes.buttonGroup}>
                        <Button variant='contained' disabled={!title || !desc} color='secondary' className={classes.oneOfButtonsGrid} onClick={()=>setUploadModal(true)}>
                            <Typography variant="button" display="block" gutterBottom>Загрузить картинку</Typography>
                        </Button>
                    </Grid>}
                    {loading && <CircularProgress className={classes.progress} />}
                </Grid>
            </Paper>
            <UploadModal
                setUploadModal={setUploadModal}
                changeTitle={changeTitle}
                changeDesc={changeDesc}
                uploadModalOpen={uploadModalOpen}
                desc={desc}
                title={title}
                loading={loading}
                changeLoading={changeLoading}
            />
        </main>
    );
}

AdminImageUploadComponent.propTypes = {
    changeTitle: PropTypes.func,
    title: PropTypes.string,
    changeDesc: PropTypes.func,
    desc: PropTypes.string,
    loading: PropTypes.bool,
    changeLoading: PropTypes.func
};





export default AdminImageUploadComponent;