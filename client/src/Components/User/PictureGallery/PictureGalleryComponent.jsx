import React, {useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    GridList,
    GridListTile,
    Button,
    Typography,
} from '@material-ui/core';
import clsx from "clsx";
import PropTypes from "prop-types";


const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        height: '100%',
        backgroundColor: theme.palette.primary.light,
        paddingTop: 50,
        transition: 'transform .3s'
    },
    gridList: {
        backgroundColor: 'inherit',
        width: '100%',
        height: '100%',
        overflow: 'hidden',
    },
    dwnlBtn: {
        fontWeight: 900,
        transition: 'all .2s',
        margin: '25px 0 90px 0',
        '&:hover': {
            transform: 'translateY(-3px)',
            boxShadow: '0 1rem 2rem rgba($color-black,.2)',
            '&::after': {
                transform: 'scaleX(1.4) scaleY(1.6)',
                opacity: 0
            }
        },
    },
    image: {
        position: 'relative',
        display: 'block',
        top: '50%',
        width: '100%',
        transform: 'translateY(-50%)',
    },
    tile:{
        webkitBackgroundClip: 'text',
        '&:hover': {
            cursor: 'pointer',
            transform:'translateY(-2px) scale(1.01)'
        },
    },
    lastTile: {
        '&:last-of-type': {
            width: '100% !important',
            marginBottom: 90,
        },
    },
    lastTileNoImagesLeft: {
        '&:last-of-type': {
            marginBottom: 90,
        },
    },
    overlayTxt:{
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%,-50%)',
    },
    imgTitle: {
        fontWeight: 900,
        color: 'white',
    },
    imageBlock:{
        height: '100%',
        display: 'block',
        overflow: 'hidden',
        position: 'relative',
        perspective: 500,
        transition:'all 0.4s ease-in',
        '&:hover $hoverDesc':{
            transform: 'translate(-50%, -50%) rotateY(0deg)',
            opacity: 1
        }
    },
    hoverDesc:{
        display: 'block',
        textAlign: 'center',
        width: '70%',
        height: '80%',
        padding: '5% 8%',
        opacity: 0,
        backgroundColor: 'rgba(162,162,162,0.9)',
        position: 'absolute',
        top: '50%',
        left: '50%',
        transformOrigin: '50%',
        content: "",
        transform:'translate(-50%, -50%) rotateY(90deg)',
        transition:'all 0.4s ease-in',
    },
    description: {
        color: 'white',
    },
    textNotFound:{
        height: '85vh',
        color: theme.palette.secondary.main,
        fontWeight: 900,
    }

}
));


const PictureGalleryComponent = props => {
    const {images, deleteImage, downloadMoreImages, noImagesLeft} = props;
    const classes = useStyles();

    //Создаем реф для каждого элемента
    const refs = images.reduce((acc, value) => {
        acc[value.id] = React.createRef();
        return acc;
    }, {});

    // Плавный скрол прт добавлении новых элементов
    useEffect(() => {
        if(Object.keys(refs).length > 4){
            setTimeout(() => {refs[images.length-1].current.scrollIntoView({
                behavior: 'smooth',
            });
            }, 250)
        }
    }, [refs, images.length]);

    return (
        <div className={classes.root} >
            {images.length > 0 ?
                <>
                    <GridList cellHeight={550} spacing={8} className={classes.gridList}>
                        {images.map(tile => (
                            <GridListTile key={tile.id} className={images.length % 2 === 0 ? classes.tile : noImagesLeft ?  clsx(classes.lastTile, classes.tile, classes.lastTileNoImagesLeft) : clsx(classes.lastTile, classes.tile)}>
                                {/*<img src={tile.src} alt={tile.title} className={classes.image}/>*/}

                                <div className={classes.imageBlock}>
                                    <img src={tile.src} alt={tile.title} className={classes.image} ref={refs[tile.id]}/>
                                    <div className={classes.overlayTxt}>
                                        <Typography variant="h1" component="h2" gutterBottom
                                                    className={classes.imgTitle}>
                                            {tile.title}
                                        </Typography>
                                    </div>
                                    <div className={classes.hoverDesc}>
                                        <h1 className={classes.imgTitle}>Описание:</h1>
                                        <p className={classes.description}> {tile.desc}</p>
                                        <Button color={"secondary"} variant={"outlined"} className={classes.dwnlBtn}
                                                onClick={() => {
                                                    deleteImage(tile.id);}

                                                }>Удалить</Button>
                                    </div>
                                </div>
                            </GridListTile>
                        ))}
                    </GridList>
                    {!noImagesLeft && <Button color={"secondary"} variant={"contained"} className={classes.dwnlBtn} onClick={()=>{
                        downloadMoreImages()
                    }} >Загрузить еще</Button>}
                </> :
                <Typography variant="h5" component="h3" className={classes.textNotFound}>
                    Не найдено ни одной картинки
                </Typography>
            }
        </div>
    );
};
PictureGalleryComponent.propTypes = {
    images: PropTypes.array,
    noImagesLeft: PropTypes.bool,
    deleteImage: PropTypes.func,
    downloadMoreImages: PropTypes.func,
};

export default PictureGalleryComponent;