import React, {Component, lazy, Suspense} from 'react';
import {BrowserRouter as Router, Redirect, Route, Switch} from "react-router-dom";
import Loading from '../Components/Loading'
import loggedIn from '../Helpers/LoggedIn'
import isUserOrAdminLocal from '../Helpers/isUserOrAdminLocal'

/*  404   */
const NotFound = lazy(() => import('../Components/NotFound'));

/*  USER ROUTES   */
const Login = lazy(() => import('../Controllers/User/Login'));
const Registration = lazy(() => import('../Controllers/User/Registration'));
const PictureGallery = lazy(() => import('../Controllers/PictureGallery'));

/*  ADMIN ROUTES   */
const AdminImageUpload = lazy(() => import('../Controllers/Admin/AdminImageUpload'));

// Роутер работает с код сплитингом с помощью React.lazy , во время подгрузки раутов показывается компонент Loading с помощью React.suspense
class Routes extends Component {
	render() {
		// Создаём кастомные приватные рауты для админа и юзера
		const PrivateAdminRoute = ({ component: Component, ...rest }) => (
			<Route {...rest} render={(props) => (
				isUserOrAdminLocal() === 'admin'
					? <Component {...props} />
					: <Redirect to='/login' />
			)} />
		);

		const PrivateAdminAndUserRoute = ({ component: Component, ...rest }) => (
			<Route {...rest} render={(props) => (
				(isUserOrAdminLocal() === 'admin' || isUserOrAdminLocal() === 'user')
					? <Component {...props} />
					: <Redirect to='/login' />
			)} />
		);
		return (
			<Router>
				<Suspense fallback={<Loading/>}>
					<Switch>
						<Route exact path="/" render={() => {
							if(loggedIn() && isUserOrAdminLocal() === 'user') return <Redirect to="/picture-gallery"/>;
							if(loggedIn() && isUserOrAdminLocal() === 'admin') return <Redirect to="/admin-image-upload"/>;
							else return <Login/>
						}}/>
						<Route path="/login" component={Login} />
						<Route path="/registration" component={Registration} />

						<PrivateAdminRoute exact path="/admin-image-upload" component={AdminImageUpload} />
						<PrivateAdminAndUserRoute exact path="/picture-gallery" component={PictureGallery}/>
						{/* 404*/}
						<Route path="*" component={NotFound} />
					</Switch>
				</Suspense>
			</Router>
		);
	}
}


export default Routes;
