export let IMAGES = 'http://localhost:9006/api/images/';
export let UPLOAD_URL = 'http://localhost:9006/api/images/upload';
export let REGISTRATION = 'http://localhost:9006/api/user/register';
export let LOGIN = 'http://localhost:9006/api/user/login';
