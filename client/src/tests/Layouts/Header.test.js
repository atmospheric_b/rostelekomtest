import React from 'react';
import { shallow } from 'enzyme';
import Header from '../../Components/Layouts/Header'

it('Header renders without crashing', () => {
    const wrapper = shallow(<Header />);
   expect(wrapper).toMatchSnapshot()
});