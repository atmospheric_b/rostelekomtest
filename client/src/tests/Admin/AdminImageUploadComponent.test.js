import React from 'react';
import { shallow } from 'enzyme';
import AdminImageUploadComponent from '../../Components/Admin/AdminImageUploadComponent';
import AdminImageUpload from '../../Controllers/Admin/AdminImageUpload/';


it('AdminImageUploadComponent renders without crashing', () => {
    const wrapper = shallow(<AdminImageUpload />);
    const wrapper2 = shallow(<AdminImageUploadComponent />);
    expect(wrapper).toMatchSnapshot();
    expect(wrapper2).toMatchSnapshot();
});


test("Admin enters image details", () => {
    const value ='hello';
    const event = {target: { value }};
    const controller = shallow(<AdminImageUpload />);
    const component = controller.find('AdminImageUploadComponent').dive();
    component.find('#title').props().onChange(event);
    component.find('#desc').props().onChange(event);
    expect(controller.state().title).toEqual(value);
    expect(controller.state().desc).toEqual(value);
});