import React from 'react';
import { shallow } from 'enzyme';
import PictureGalleryComponent from '../../Components/User/PictureGallery/PictureGalleryComponent';
import PictureGallery from '../../Controllers/PictureGallery/';

it('PictureGallery renders without crashing', () => {
    const wrapper = shallow(<PictureGallery />);
    const wrapper2 = shallow(<PictureGalleryComponent images={[]}/>);
    expect(wrapper).toMatchSnapshot();
    expect(wrapper2).toMatchSnapshot();
});
