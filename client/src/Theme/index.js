import { createMuiTheme } from '@material-ui/core/styles';
import { green, grey, red } from '@material-ui/core/colors';

const rawTheme = createMuiTheme({
	palette: {
		primary: {
			light: '#69696a',
			main: '#28282a',
			dark: '#1e1e1f',
		},
		secondary: {
			light: '#fff5f8',
			main: '#ff3366',
			dark: '#e62958',
		},
		warning: {
			main: '#ffc071',
			dark: '#ffb25e',
		},
		error: {
			xLight: red[50],
			main: red[500],
			dark: red[700],
		},
		success: {
			xLight: green[50],
			dark: green[700],
		},
	},
	typography: {
		fontSize: 20,
		fontFamily: "'Work Sans', sans-serif",
		fontWeightLight: 300, // Work Sans
		fontWeightRegular: 400, // Work Sans
		fontWeightMedium: 700, // Roboto Condensed
		fontFamilySecondary: "'Roboto Condensed', sans-serif",
	},
});

const fontHeader = {
	color: rawTheme.palette.text.primary,
	fontWeight: rawTheme.typography.fontWeightMedium,
	fontFamily: rawTheme.typography.fontFamilySecondary,
	textTransform: 'uppercase',
};

const theme = {
	...rawTheme,
	palette: {
		...rawTheme.palette,
		background: {
			...rawTheme.palette.background,
			default: rawTheme.palette.common.white,
			placeholder: grey[200],
		},
	},
	typography: {
		...rawTheme.typography,
		fontHeader,
		h1: {
			...rawTheme.typography.h1,
			...fontHeader,
			letterSpacing: 0,
			fontSize: '6rem',
		},
		h2: {
			...rawTheme.typography.h2,
			...fontHeader,
			fontSize: '4.8rem',
		},
		h3: {
			...rawTheme.typography.h3,
			...fontHeader,
			fontSize: '4.2rem',
		},
		h4: {
			...rawTheme.typography.h4,
			...fontHeader,
			fontSize: '3.6rem',
		},
		h5: {
			...rawTheme.typography.h5,
			fontSize: '2rem',
			fontWeight: rawTheme.typography.fontWeightLight,
		},
		h6: {
			...rawTheme.typography.h6,
			...fontHeader,
			fontSize: '2.8rem',
		},
		subtitle1: {
			...rawTheme.typography.subtitle1,
			fontSize: '2.8rem',
		},
		body1: {
			...rawTheme.typography.body2,
			fontWeight: rawTheme.typography.fontWeightRegular,
			fontSize: '1.6rem',
		},
		body2: {
			...rawTheme.typography.body1,
			fontSize: '1.4rem',
		},
	},
};

export default theme;