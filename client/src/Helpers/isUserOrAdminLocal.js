
const isUserOrAdminLocal = () => {
    const access_token = localStorage.getItem('access_token');
    const user = JSON.parse(localStorage.getItem('user'));
    if(access_token && user){
        if(user.email === 'admin@admin.com') return 'admin';
        else return 'user';
    }
    else{
        return null
    }
};

export default isUserOrAdminLocal;
