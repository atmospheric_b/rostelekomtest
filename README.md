# Ростелеком тест задание Столяров Дмитрий Андреевич "Галерея"

* [Репозиторий](https://gitlab.com/atmospheric_b/rostelekomtest) - gitlab


Проект содержит как бекенд так и фронтенд.

## Для запуска
Установить пакет Yarn и Nodejs 

https://yarnpkg.com/en/docs/install

https://nodejs.org/en/ 

Далее переходим в папку server проводим установку модуля для одновременного запуска бекенда и фронта
```
 cd server\
 yarn conc
```

Проводим установку нод модулей проекта

```
 yarn initInstall
```

Запускаем проект
```
 yarn dev
```
### База данных

Этот проект использует файловую базу дынных node-json-db
В директории ./server/db

Я предусмотрел бэкап бд.
Чтобы восстановить бд к изанчальному состоянию надо находясь в директории server выполнить команду

Я подгрузил в базу 20 рандомных изображений.

```
 yarn restoreDb
```

### Авторизация и регистрация

Авторизация реализована через логин и пароль и JWT токен!
Пароли хэшируются
Я сделал  в базе одного админа. Изменять или добавлять его нельзя! 

Аккаунт админа:
```
"email": admin@admin.com,
"password": admin,
```

При регистрации происходит валидация правильного значения email,
а также валидация совпадения пароля и повтора пароля.

После авторизации пользователя кидает на соответсвующие его роли рауты:
пользователя в галерею, а админа в загрузку картинок

### Серверная часть

Серверная часть реализована на nodejs express.
Работает на 9006 порту
Api предусматривает два endpoint`а: 

/images для картинок   
/user для пользователей 

Используются различные middleware для компрессии данных и решения проблем cors

Архитектура папок предусматривает возможности к удобному масштабированию проекта.

Проект по желанию можно запустить через Docker.
Файл Dockerfile я приложил.

Я написал пару тестов для проверки endpoint /images на выдачу валидных данных при запросе на этот api
запустить их можно находясь в папке /server командой

```
yarn test
```

### Клиентская часть

Проект выполнен на React 16.8 c заботой об ie9+ как предпологало задание!
Архитектура проекта позволяет удобно масштабировать проект под свои нужды!
Основные стили выполнены на библиотеке Material Ui.
Тема стилей находится в папке Themes. 

Я ипользую конфигурацию с урлами апи в config.js. 

Для расшифровки JWT использую переменную среды в файле .env - 
на продакшене её надо убрать в .gitignore

Проект по желанию можно запустить через Docker.
Файл Dockerfile я приложил. 

В проекте реализованы две роли(user и админ) и навигация с помощью React Router Dom! 

Бандл js и css собирается с помощью webpack зашитого в модуль react-scripts 
Для правки конфигурации webpack и зависмостей можно выполнить команду, но действие будет не обратимо

```
yarn eject
```
 В проекте реализована асинхронная подгрузка компонентов и код сплитинг(в основном в роутах) с помощью
 React.lazy и React.suspense
 
 Рауты react-router-dom защищены проверко наличия токена у пользователя в localstorage 
 ,а также зависимость от роли пользователя(user admin).
 
 В интерфейсе предусмотрена навигация с помощью меню(поддерживается свайп)
 Дизайн выполнен в responsive стиле.
 
 Пользователь может только просматривать и удалять картинки. 
 Кнопкой "загрузить еще" происходит подгрузка иозбражений из бд, пока в ней они есть.
 При получении новых изображений происходит плавный скролл на новый блок картинок.
 При отсутсвии изображений в бд выдается предупреждение и кнопка подгрузки пропадает.
 
 Админ помимо просмотра изображений как пользователь , может заружать картинки по одной в бд.
 Загрузка реализована с выпадающей модалкой и превью.
 
 У юзера и админа есть возможность выйти из акканта кнопкой выхода в хедере или в меню.
 
 Так как я решил использовать flex в grid , изображения я расположил по 2 колонны. При удалении одной из картинок,
 все картинки смещаются ,а последняя картинка будет занимать 2 колонны сразу. 

 В своей архитектуре я использую модель controller(и модель)->view
 В папке Controllers я распологаю классовые компоненты , где сидит вся логика. Далее стейт и логику я передаю через пропсы
 в функциональные компоненты(тупые) находящиеся в папке Components. Ввиду того, что в последних версиях реакт у функциональных компонентов
 появились useState, useEffect моя архитектура усложняется, но при возможности я стараюсь основную логику
 проводить в контроллерах.
 Так же в папке Components лежат различные лейауты, 404 страница и модалка.
 Во всех компонентах, где необходимо выполнил проверку через Prop-Types. 
 
 Для связи с апи использую модуль axios , c помощью хелпера Helpers/apiRequest.js определаю по дефолту заголовок в запросах
 'Authorization = Bearer token'

## Тесты

Написал базовые тесты на валидный рендеринг компонентов со сверкой по скриншотам. А также проверку на ввод данных юзером или админом.
Запустить их можно находясь в папке ./client командой

```
yarn test
```

## Мысли
Проект состоит из двух папок server и client со своими отдельными node_modules и packages.json для того 
,чтобы при желание можно было бы легко разнести их на отдельные сервера допустим. Но при этом надо будет создать отдельные репозитории,
так как сейчас репозиторий один на обе папки.

При старте работы над проектом я долго не мог решить как мне реализовать хранение картинок на сервере.
Приходили в голову такие варианты как Redis или MongoDb - но они требовали дополнитльных установко, а мне хотелось выполнить проект
максимально независимо от сторонних программ.
Были мысли о Google Firebase и AWS S3 - но проблема в том, что они требуют аккаунт и ключи, что тоже в нашем с Вами случаем - не удобно.
Так что мой выбор пал на бд в виде файла.

Так как хранить файлы в бд - плохая идея. Я сохраняю изображения в директории на сервере /imgages , а в бд храню ссылку на них и информацию.

Разрабатываю свои проекты, я всегда стараюсь их делать максимально масштабируемыми с оглядкой на возможное будущее.

Потратил в целом на задание я наверное часов 20, но это не точно)

В целом задание интересное и было бы здорово услышать от Вас фидбек!

p.s. решил в конце сделать чтобы проект можно было запустить и из ./client и ./server командой

```
 yarn dev
```



